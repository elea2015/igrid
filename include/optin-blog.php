<div class="blog third optin">
    <div class="blogImg">
        <img src="<?php echo get_template_directory_uri(); ?>/img/optin.jpg" alt="">
    </div>
    <div class="blogBody text-center">
        <h4>TikTok Marketing Hacks</h4>
        <p>Get the latest updates, tips, trends, and hacks for TikTok delivered straight to your inbox!</p>
        <div class="ctaRow">
            <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]')?>
        </div>
    </div>
</div>