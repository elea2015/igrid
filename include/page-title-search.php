<div class="graphic_container m-auto">
    <h1 class="text-center py-3 mt-5 homeTitle">TikTok Marketing Tips</h1>
        <img class="graphic_item_left" src="<?php echo get_template_directory_uri()?>/img/ig-left.svg" alt="">
        <img class="graphic_item_right" src="<?php echo get_template_directory_uri()?>/img/ig-right.svg" alt="">
        <div class="blogConteiner">
        <div id="searchFormContainer" class="py-3">
            <?php get_template_part('searchform'); ?>
        </div>
    </div>
</div>