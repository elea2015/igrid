<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">

		<!-- TrustBox script -->
		<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
		<!-- End TrustBox script -->

		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">


		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/icons/site.webmanifest">
		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/icons/safari-pinned-tab.svg" color="#4ed687">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">

    	<!-- <link rel="stylesheet" type="text/css" href="https://use.typekit.net/whj2hjh.css" /> -->

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	   <!--  <meta name="description" content="<?php //bloginfo('description'); ?>">  -->
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        //conditionizr.config({
           // assets: '<?php echo get_template_directory_uri(); ?>',
        //    tests: {}
        //});
        </script>
        <!-- font awesome -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
		<!-- google fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap" rel="stylesheet">
		<!-- autopilothq -->
		<script type="text/javascript">(function(o){var b="https://api.autopilothq.com/anywhere/",t="604a4077ad504cef9ba4a6c4796c5fcb639ff3977562450688b58e6dca07d8e9",a=window.AutopilotAnywhere={_runQueue:[],run:function(){this._runQueue.push(arguments);}},c=encodeURIComponent,s="SCRIPT",d=document,l=d.getElementsByTagName(s)[0],p="t="+c(d.title||"")+"&u="+c(d.location.href||"")+"&r="+c(d.referrer||""),j="text/javascript",z,y;if(!window.Autopilot) window.Autopilot=a;if(o.app) p="devmode=true&"+p;z=function(src,asy){var e=d.createElement(s);e.src=src;e.type=j;e.async=asy;l.parentNode.insertBefore(e,l);};y=function(){z(b+t+'?'+p,true);};if(window.attachEvent){window.attachEvent("onload",y);}else{window.addEventListener("load",y,false);}})({});</script>
	</head>
	<body <?php body_class(); ?>>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153709003-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-153709003-1');
	</script>

	<!-- klaviyo Web Tracking -->
	<script async type="text/javascript" src="//static.klaviyo.com/onsite/js/klaviyo.js?company_id=RMcJFj"></script>

	  <!-- Load Facebook SDK for JavaScript -->
	  <div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>

		<div id="mainHeader" class="header sticky-top">
			<header class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light px-0">
				<a class="navbar-brand" href="https://influencegrid.com/">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-influencegrid.svg" alt="">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<?php
				wp_nav_menu([
				'menu'            => 'top',
				'theme_location'  => 'header-menu',
				'container'       => 'div',
				'container_id'    => 'bs4navbar',
				'container_class' => 'navbar-nav m-auto',
				'menu_id'         => false,
				'menu_class'      => 'navbar-nav ml-auto',
				'depth'           => 2,
				'fallback_cb'     => 'bs4navwalker::fallback',
				'walker'          => new bs4navwalker()
				]);
				?>
				<?php
		        	$emailField = ($_GET["emailField"]);
		    	 ?>
				<a class="btn btn-primary" href="https://influencegrid.com/pricing">Start Discovering</a>
				</div>
			</nav>
			</header>
		</div>
