<?php /* Template name: Home*/ get_header(); ?>

<main role="main">
    <?php require('include/page-title-search.php'); ?>
    <!-- Recent section -->
    <section class="blogList">
        <div class="blogContainer"><?php require('include/recent-post.php'); ?></div>
    </section>
    <!-- Popular section -->
    <h3 class="text-center py-3 mt-2 homeTitle">Most Popular</h3>
    <section class="blogList">
        <div class="blogContainer"><?php require('include/custom-popular-post.php'); ?></div>
        <div class="text-center my-4">
            <a href="/recent" class="btn btn-primary">See More Blogs</a>
        </div>
    </section>
    <!-- Cats section -->
    <h3 class="text-center py-3 mt-5 homeTitle">Browse Blogs by Category</h3>
    <section class="blogList">
    
        <div class="blogContainer"><?php require('include/cat-list.php'); ?></div>
    </section>
</main>

<?php get_footer();?>