<footer>
  <div class="container">
    <div class="footerFlex">
      <div class="mb-3 px-0 footer1">
        <a href="https://influencegrid.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-influencegrid.svg" alt=""></a>
        <p class="mt-2">Made with ❤️ in San Diego, CA</p>      
      </div>
      <div class="footer2">
        <ul class="list-unstyled">
          <li><a href="https://influencegrid.com/pricing">Pricing</a></li>
          <li><a href="https://influencegrid.com/contact">Contact</a></li>
          <li><a href="https://influencegrid.com/contact">Help</a></li>
          <li><a href="https://feedback.influencegrid.com/feature-requests">Feedback</a></li>
          <li><a href="https://influencegrid.com/terms-of-service">Terms of Service</a></li>
          <li><a href="https://influencegrid.com/privacy-policy">Privacy Policy</a></li>
          <li><a href="https://influencegrid.com/tiktok-money-calculator">TikTok Money Calculator</a></li>
        </ul>
      </div>
      <div class="footer3">
        <ul class="list-unstyled">
          <li><a href="https://influencegrid.com/tiktok-search-engine">TikTok Search Engine</a></li>
          <li><a href="https://influencegrid.com/find-tiktok-influencers-by-location">Find TikTok Influencers by Location</a></li>
          <li><a href="https://influencegrid.com/find-tiktok-influencers-in-your-niche">Find TikTok Influencers in Your Niche</a></li>
          <li><a href="https://influencegrid.com/tiktok-username-search">TikTok Username Search</a></li>
          <li><a href="https://influencegrid.com/tiktok-email-finder">TikTok Email Finder</a></li>
          <li><a href="https://influencegrid.com/tiktok-influencer-list">TikTok Influencers List</a></li>
          <li><a href="https://influencegrid.com/tiktok-influencer-analytics">TikTok Influencer Analytics</a></li>
        </ul>
      </div>
      <div class="footer5">
        <p>Get Advanced TikTok Marketing Strategies</p>
        <div class="ctaSubscribe ctaSubscribeP">
            <div class="ctaRow">
                <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]')?>
            </div>
        </div>
     
      <!-- social -->
      <div class="socialFooter mt-2">
        <p>Follow us</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="https://www.instagram.com/kicksta.co/" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/img/Instagramicon.svg" alt="">
            </a>
          </li>
          <li class="list-inline-item">
            <a href="https://www.facebook.com/Kicksta.co/" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/img/FBicon.svg" alt="">
            </a>
          </li>
        </ul>
      </div>
      <!-- social -->
      </div>
    </div>
  </div>
</footer>


<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/infinite.js"></script>
<!--  <script src="<?php //echo get_template_directory_uri(); ?>/js/myscript.js"></script> -->

<script>
  window.intercomSettings = {
    app_id: "hg32et9j"
  };
</script>
<!-- <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/hg32et9j';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<script src="//rum-static.pingdom.net/pa-5b463bfcef13ce0016000164.js" async></script> -->
	</body>
</html>
