<?php get_header(); ?>

	<main role="main">
		<?php get_template_part('include/page-title-search'); ?>	
		
		<!-- Recent section -->
		<section class="blogList">
			<?php get_template_part('customloopv2'); ?>
		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
