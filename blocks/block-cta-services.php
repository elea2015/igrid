<div class="cta-service__container">
    <div class="cta-service__image">
        <img src="<?php echo get_template_directory_uri()?>/img/logo-square.png">
    </div>
    <div class="cta-service__headline">
        <h4><?php block_field( 'heading' ); ?></h4>
        <a class="btn btn-primary" href="https://influencegrid.com/pricing">Try Influence Grid Now!</a>
    </div>
</div>