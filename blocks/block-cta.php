<div class='ctaJoin container'>
<style>
    .automaticCTA{
        display:none;
    }
</style>
    <div class='ctaRow'>
        <ul class='list-inline'>
            <li class='list-inline-item emoji'>🤩</li>
            <li class='list-inline-item emoji'>🤙</li>
            <li class='list-inline-item emoji'>🤑</li>
        </ul>
        <h4 style="margin-bottom:24px"><?php block_field( 'heading' ); ?></h4>
        <p><?php block_field( 'sub-header' ); ?></p>
        <form class='formStarted d-none my-2 my-lg-0 getStartedForm mx-auto marketerForm' method='post' action='https://kicksta.co/pricing'>
            <input class='form-control mx-0 emailField' name='email' type='email' placeholder='Your Email'>
            <button class='btn btn-primary mx-0' type='submit'>Get Started</button>
        </form>
        <div class="ctaRow">
            <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]')?>
        </div>
    </div>
</div>