<style>
    .quotes-alt{
        padding:15px;
    }
    .quotes-alt p {
        font-style: italic;
        font-weight:bold;
        font-size:25px !important;
    }
</style>
<div class="quotes-alt">
    <p><?php block_field( 'quote' ); ?></p>
</div>