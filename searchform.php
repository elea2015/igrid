<!-- search -->

<form id="searchForm" class="form-inline my-2 my-lg-0 mx-auto justify-content-center" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="form-control mx-0" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'html5blank' ); ?>">
	<button class="btn btn-dark" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>
<!-- /search -->
