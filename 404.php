<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<!-- article -->
			<article id="post-404" class="text-center py-5 my-5">

				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				<h2>
					<a class="btn btn-primary" href="<?php echo home_url(); ?>"><?php _e( 'Return to blog?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
